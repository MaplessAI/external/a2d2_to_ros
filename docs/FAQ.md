# FAQ for interpreting the A2D2 data set

This document contains a set of questions that might be unclear after examining readme file data at this page:

https://www.a2d2.audi/a2d2/en/download.html

And the technical description of the data set at this page:

https://arxiv.org/abs/2004.06320

Answers, where available, are given directly below the questions and have come from discussions with the [A2D2 Team](https://www.a2d2.audi/a2d2/en/team.html). If a question has no answer below it, then it remains an open question.

## General

1. **Why do converters only run on portions of the data set instead of the whole thing?**
    * *The data set is quite large (~2.3TB). Many people won't be able to download and convert the whole thing at once, which would likely require over 5TB of free storage.*
1. **What convention do the timestamps follow?**
    * *All timestamps encode [TAI](https://en.wikipedia.org/wiki/International_Atomic_Time) microseconds since Epoch begin.*
1. **What is the precision of real-valued data (e.g., single precision, double precision, etc.)?**
    * *None of the data in the data set was recorded with greater than single precision. (For lidar data, note that numpy stores the point data with double precision, but the data itself is only generated with single precision.)*
1. **Are the labeled data a subset of the unlabeled data, or are they different sets?**
    * *The object detection dataset is a subset of the semantic segmentation dataset. The raw sensor fusion data (Gaimersheim, Ingolstadt, and Munich) is a different set.*
1. **Do all subsets in the unlabeled data set span the same time?**
    * *No, there are slightly different start times among the sensor locations. For example, the table below lists start times for the Ingolstadt logs (largest magnitude times in bold):*
1. **How do I synchronize bus signal messages using [message filters](http://wiki.ros.org/message_filters)?**
    * *Since ROS does not provide stamped Float message types, the easiest thing to do is probably to define a custom stamped type and then use the [rosbag API](http://wiki.ros.org/rosbag/Code%20API) to convert the sensor bus bag files to use your custom stamped type. The time value that should be used for the header stamp is the timestamp of the message in the bag file. The A2D2 converters all use the convention that the message timestamps are the same as the header time stamps.*

## Point cloud conventions

1. **What is a 'lidar frame'?**
    * *A 'lidar frame' comes from a pseudo sensor that contains point cloud data represented as if it were collected by a camera on the vehicle. There is a pseudo lidar sensor for each camera on the collection vehicle. Each of these pseudo sensors operates at the same frequency as its corresponding camera so that a lidar frame is generated for each camera frame. To build a lidar frame, lidar points are accumulated from all lidar sensors across a short window of time around the camera frame and then projected into the camera frame. Points that project onto the camera image are stored in a "frame" of lidar data.*
1. **Is any motion correction done for the lidar points in each lidar frame?**
    * *Yes, the point cloud points are spatially corrected for roll and pitch, and are published in the wheels frame. However, the points are not temporally corrected so slight misalignment may remain. Each point carries a `rectime` attribute that has the exact time at which the point was recorded, and this can be used in conjuntion with bus signal data to perform any desired temporal correction.*
1. **Why is there no timestamp for the lidar frames?**
    * *Because the points in each lidar frame are not temporally aligned (see also previous question). For the sake of visualization, the data set converter assigns each lidar frame the timestamp from the corresponding camera frame. But for any operations or analysis on the data you should use the `rectime` attribute of each lidar point.*
1. **Are the pseudo lidar sensors spatially aligned with the camera sensors?**
    * *Yes and no. The transform tree between the two is identical, but the camera tree is rooted in chassis frame and the pseudo lidar tree is rooted in the wheels frame. This means that the `(x, y, z)` values of the lidar points have been corrected for roll and pitch of the chassis with respect to the wheels, but the camera images have not. However, the attributes of the lidar point `depth`, `distance`, and `azimuth` are with respect to the camera sensors and not the pseudo lidar sensors. These values can only be interpreted by using the `row` and `col` attributes of the lidar points to look up the camera image pixel corresponding to a given lidar point. In other words, those `row` and `col` attributes implicitly provide the transform between pseudo lidar sensor and camera sensor.*
1. **What is the motivation for representing lidar data in "frames" that are camera frame aligned and logged at camera frequency?**
    * *Many use cases for the data involve machine learning, and many of those use cases operate primarily in the camera frame. The idea was to simplify the data ingestion process for those types of uses.*
1. **Do lidar frames contain duplicate points over time?**
    * *Yes, because the lidars publish at a lower frequency than the cameras. For example, two sequential lidar frames from the `front_center` sensor may contain the same point cloud point.*
1. **Do lidar frames contain duplicate points over space?**
    * *Yes, because the frames correspond to camera fields of view, and the camera fields of vew may overlap. For example, the `front_center` camera FOV overlaps with the `front_left` camera FOV, so at any given point in time, lidar frames for those two sensors may contain equal point cloud points.*

| Sensor       | Start time        |
| :------------| :---------------: |
| Bus signals  | 1554121593489120  |
| Front center | 1554121593909522  |
| Front left   | 1554121593909500  |
| Front right  | 1554121593909500  |
| Rear center  | 1554121595034993  |
| Side left    | **1554121595035037**  |
| Side right   | **1554121595035037**  |

## Conventions

1. **Are all units in the `cams_lidars.json` file SI?**
    * *Distances are in meters, timestamp_delay is in microseconds*
1. **Are coordinate systems right handed?**
    * *Yes.*
1. **In the bus signal data, what are the `roll_angle` and `pitch_angle` conventions?**
    * *Euler/Tait-Bryan angles that describe the offset from the wheels frame to the chassis frame, with sign convention from DIN 9000.*
1. **What are the conventions for the reference frame ***g*** (described in [Section 3.1](https://arxiv.org/pdf/2004.06320.pdf))?**
    * *The frame ***g*** is fixed with respect to the chassis of the vehicle, and all sensor poses are static with respect to it.*
    * *In addition, there is a `wheels` frame that, at rest, is coincident with ***g***. In motion, ***g*** can roll and pitch (but not yaw or translate) with respect to `wheels`. In the bus signal data, the `roll_angle` and `pitch_angle` values describe these offsets.*

## Sensor configuration

1. **The tutorial states that `tstamp_delay` "specifies a known delay in microseconds between actual camera frame times"; what does that mean? Are these delays accounted for in the frame timestamps?**
    * *This is baked for the semantic segmentation and object detection datasets but not for the raw datasets. You can use this delay to register lidar/camera timestamps. Lidar comes before camera, and therefore tstamp_delay correction is optionally needed here.*
1. **What are the `CorrectedImagePort` fields in `cams_lidars.json` for the `side_left` and `rear_center` cameras? Why are they `null`?**
    * *These fields are not used and can be ignored.*
1. **What are the `view_` fields in the camera sections of `cams_lidars.json`?**
    * *These fields are not used and can be ignored.*

## Sensor fusion bus signal data

1. **There is no `yaw_angle` field; is there supposed to be?**
    * *No. See the reference frame ***g*** topic in the 'Conventions' section.*
1. **What are the `distance_pulse_*` fields and what do the values represent?**
    * *Distance pulse data comes from pulse counter at the wheels. Counters generally run forward, even when driving backwards. If the sensor is reset due to error or failure, the value is always, without exception, to send 1 times the Init value 1021. If the value 1021 is not present in the data set, then there were resets due to error or failure.*
1. **What are the `latitude_direction` and `longitude_direction` fields and what do the values represent?**
    * *latitude_direction: North/South Hemisphere (0 north / 1 south), longitude_direction: East/West Hemisphere (0 east, 1 west)*
1. **What are the conventions for the `steering_angle_calculated` values (i.e., what are min and max, and what is centered)?**
    * *Steering angle with straight-line flow correction from the position of the steering motor (0 = centered, stepsize = 0.15, min = 0, max = 614.25, init = 614.10, error = 614.25).*
1. **What are the conventions for the `*_sign` fields?**
    * *0 = left/positive, 1 = right/negative*
1. **Is `vehicle_speed` allowed to be negative?**
    * *No, the vehicle_speed is always between 0 and 655.35.*
1. **What is the convention for `accelerator_pedal` percent values (i.e., is 0 or 100 fully depressed)?**
    * *100 = fully depressed*

## Sensor fusion lidar data

1. **Are the points in the point cloud ordered in any way? If so, what is the ordering?**
    * *There is no ordering to the data in each lidar frame.*
1. **What is the `pcloud_attr.rectime` lidar data field?**
    * *The 'rectime' field contains the recording time when this data point was recorded.*
1. **What is the `pcloud_attr.timestamp` lidar data field?**
    * *The 'timestamp' field contains the time when the operating system received the TCP/IP package from the device.*
1. **What is the `pcloud_attr.boundary` lidar data field?**
    * *Boundary flag is true if the data point is coming from bordering lidar channel, probably ring 0 and ring 15 in the data set (recorded with VLP16).*
1. **What is the `pcloud_attr.valid` lidar data field?**
    * *Valid is only true for points that need to be considered. There are some points in the xy plane near the sensor that need not be considered. That is, if valid != true, don’t consider this point.*
1. **What are the units for `pcloud_attr.reflectance`?**
    * *See Velodyne VLP16 manual Sec 6.1*
1. **Are `pcloud_attr.depth` and `pcloud_attr.distance` strictly non-negative?**
    * *Yes.*
1. **Are `pcloud_attr.row` and `pcloud_attr.col` supposed to be non-negative? What is the convention to convert to integer pixel coordinates?**
    * *They may be negative but then they would fall outside the image, and thus not really usable. You may choose whatever pixel conversion convention makes sense for the application you have in mind. Typically they are simply rounded to the nearest integer.*
1. **Are the lidar points in the raw sensor fusion data set motion compensated already?**
    * *Yes.*

## Semantic segmentation bus signal data

1. **What is the `driving_direction` field and what do the values represent?**
    * *The signal driving_direction indicates reverse driving (0 = direction not definined, 1 = foreward, 2 = reverse, 3 = standstill). Note that in the raw sensor fusion data set there is no reverse driving so the 'driving_direction' is not needed and not included in the bus signal data.*
1. **What do the values for the `gear` field represent?**
    * *0 = Gear N, 1 = Gear 1, 2 = Gear 2, ..., 8 = Gear 8, 9 = Automatic P, 10 = Automatic forward S, 11 = Automatic forward D, 12 = Intermediate position, 13 = reverse gear, 14 = gear not defined, 15 = Error*
1. **Are `steering_angle` and `steering_angle_sign` ground truth with respect to `steering_angle_calculated` and `steering_angle_calculated_sign`?**
    * *steering_angle is the position / shift from the middle measured on the steering link (it's no angle -> bad naming) and steering_angle_calculated is the steering wheel angle.*

## Discrepancies with online tutorial

These items have to do with variations between the data and the [tutorial](https://www.a2d2.audi/a2d2/en/tutorial.html).

### Questions

1. **The JSON info file associated with each camera image has the additional fields `image_zoom` and `pcld_view` that are not listed in the tutorial; what are they?**
    * *These fields do not provide useful information and can be ignored.*
1. **What is the purpose of the `get_axes_of_a_view` method? Why would the `x-axis` and `y-axis` members of the `view` objects not already be orthonormal?**
    * *The axes might already be orthogonal; the method is just being pedantic.*

### Notes

The tutorial lists the lidar data fields as:

```
['azimuth', 'row', 'lidar_id', 'depth', 'reflectance', 'col', 'points', 'timestamp', 'distance']
```

However, in the data set they are:

```
['pcloud_points', 'pcloud_attr.rectime', 'pcloud_attr.azimuth', 'pcloud_attr.reflectance', 'pcloud_attr.boundary', 'pcloud_attr.lidar_id', 'pcloud_attr.timestamp', 'pcloud_attr.valid', 'pcloud_attr.row', 'pcloud_attr.col', 'pcloud_attr.distance', 'pcloud_attr.depth']
```
